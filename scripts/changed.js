const { exec } = require("child_process");

console.log("Проверка неподнятых версий изменённых пакетов");

exec("npx lerna changed --include-merged-tags", (_, stdout) => {
  /** Если нашли изменённый пакет - прерываем выполнение скрипта */
  if (stdout) {
    const packages = stdout.replaceAll("\n", " ");
    throw new Error(`Найдены не поднятые версии пакетов: ${packages}`);
  }
});
