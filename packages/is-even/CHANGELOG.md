# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.8](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-even@2.0.7...@libs/is-even@2.0.8) (2024-02-29)


### Bug Fixes

* **isEvend:** добавил параметр bar ([222403d](https://gitlab.com/artyomSultanov/test-lerna/commit/222403db2ca4911e30d242ea4420c46342c57fca))





## [2.0.7](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-even@2.0.6...@libs/is-even@2.0.7) (2024-02-29)


### Performance Improvements

* **isEven:** change type param\nBREAKING CHANGE: change type param in isEven ([6769a1c](https://gitlab.com/artyomSultanov/test-lerna/commit/6769a1c1ef137043d7e03ae3be46bcf6d0b105a3))





## [2.0.6](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-even@2.0.5...@libs/is-even@2.0.6) (2024-02-29)


### Bug Fixes

* **isEven:** string param ([70a7b77](https://gitlab.com/artyomSultanov/test-lerna/commit/70a7b777ac9e2ead31fd85de29fba0ad72b4a37f))





## [2.0.5](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-even@2.0.0...@libs/is-even@2.0.5) (2024-02-29)


### Performance Improvements

* **isEven:** delete foo param ([b81cf43](https://gitlab.com/artyomSultanov/test-lerna/commit/b81cf436905faf9f76f83852de9e7d78735cd552))





## [2.0.4](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-even@2.0.0...@libs/is-even@2.0.4) (2024-02-29)


### Performance Improvements

* **isEven:** delete foo param ([b81cf43](https://gitlab.com/artyomSultanov/test-lerna/commit/b81cf436905faf9f76f83852de9e7d78735cd552))





## [2.0.3](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-even@2.0.0...@libs/is-even@2.0.3) (2024-02-29)


### Performance Improvements

* **isEven:** delete foo param ([b81cf43](https://gitlab.com/artyomSultanov/test-lerna/commit/b81cf436905faf9f76f83852de9e7d78735cd552))





## [2.0.2](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-even@2.0.0...@libs/is-even@2.0.2) (2024-02-29)


### Performance Improvements

* **isEven:** delete foo param ([b81cf43](https://gitlab.com/artyomSultanov/test-lerna/commit/b81cf436905faf9f76f83852de9e7d78735cd552))





## [2.0.1](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-even@2.0.0...@libs/is-even@2.0.1) (2024-02-29)

**Note:** Version bump only for package @libs/is-even
