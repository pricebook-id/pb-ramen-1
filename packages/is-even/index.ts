
export const isEven = (s: number, bar?: boolean): boolean => s % 2 === 0 && !bar