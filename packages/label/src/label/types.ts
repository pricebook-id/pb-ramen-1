import type { ReactNode } from 'react';

export interface LabelProps {
  children: ReactNode;

  testId?: string;
}
