import React from 'react';
import cn from 'classnames';
import styles from './styles.module.css';

import { LabelProps } from './types';

const Label: React.FC<LabelProps> = ({
  children,
  testId
}) => {

  return (
    <div
      data-testid={testId}
      className={cn(styles.label)}
      role="label"
    >
      <span>{children}</span>
    </div>
  );
};

Label.displayName = 'Label';

export default Label;
