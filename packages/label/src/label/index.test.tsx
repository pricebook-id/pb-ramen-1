import Label from '.';

import { render, screen } from '@testing-library/react';
import { describe, it, expect } from 'vitest';

describe('Test Label', () => {

  it('match snapshot for scrollable tabs', () => {
    const { asFragment } = render(
      <Label
        testId='testing-label-1'
      >
        Test
      </Label>
    );

    expect(asFragment()).toMatchSnapshot();
  });

  it('should show label test', () => {
    render(
      <Label
        testId='testing-label'
      >
        Test
      </Label>
    );

    const textLabel = screen.getByTestId('testing-label');
    expect(textLabel.textContent).toEqual('Test');
  });
});