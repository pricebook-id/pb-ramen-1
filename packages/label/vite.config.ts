import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

interface MyViteConfig {
  plugins: any[];
  esbuild?: {
    // other esbuild options here
  };
  test: {
    threads: boolean;
    testTimeout: number;
    environment: string;
    coverage: {
      reporter: string[];
    };
  };
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  test: {
    threads: false,
    testTimeout: 20000,
    environment: 'jsdom',
    coverage: {
      reporter: ['text', 'cobertura', 'lcov', 'json']
    },
  },
} as MyViteConfig);