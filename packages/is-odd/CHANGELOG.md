# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [5.0.3](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@5.0.2...@libs/is-odd@5.0.3) (2024-02-29)

**Note:** Version bump only for package @libs/is-odd





## [5.0.2](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@5.0.1...@libs/is-odd@5.0.2) (2024-02-29)


### Bug Fixes

* **isOddd:** добавил параметр foo ([d40cd1f](https://gitlab.com/artyomSultanov/test-lerna/commit/d40cd1f6ef1fd77ac855baf29155bdc2ea785611))





## [5.0.1](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@5.0.0...@libs/is-odd@5.0.1) (2024-02-29)


### Bug Fixes

* **isOdd:** поправил тип параметра на число ([ec8e932](https://gitlab.com/artyomSultanov/test-lerna/commit/ec8e93203f1d1d5e19e402195d1ca1ddfe098cb8))





# [5.0.0](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@4.5.2...@libs/is-odd@5.0.0) (2024-02-29)


### Bug Fixes

* **isOdd:** change param to string ([7b1d135](https://gitlab.com/artyomSultanov/test-lerna/commit/7b1d1357720aef22869809a2079c5081bab7d613))


### BREAKING CHANGES

* **isOdd:** smth break





## [4.5.2](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@4.5.1...@libs/is-odd@4.5.2) (2024-02-29)


### Bug Fixes

* **isOdd:** change param type for isEven func ([13a8fd8](https://gitlab.com/artyomSultanov/test-lerna/commit/13a8fd8b9e6e3d8df2dae9b7780029f047a6cb86))





## [4.5.1](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@4.5.0...@libs/is-odd@4.5.1) (2024-02-29)


### Bug Fixes

* **isOdd:** string param in isEven ([f82b586](https://gitlab.com/artyomSultanov/test-lerna/commit/f82b586ffd5fce3c73cf431a4ab128d73a448722))





# [4.5.0](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@4.0.0...@libs/is-odd@4.5.0) (2024-02-29)


### Features

* **isOdd:** b params ([d726f93](https://gitlab.com/artyomSultanov/test-lerna/commit/d726f93874a415222c9f1cced6bc6bb8016ff1d3))





# [4.4.0](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@4.0.0...@libs/is-odd@4.4.0) (2024-02-29)


### Features

* **isOdd:** b params ([d726f93](https://gitlab.com/artyomSultanov/test-lerna/commit/d726f93874a415222c9f1cced6bc6bb8016ff1d3))





# [4.3.0](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@4.0.0...@libs/is-odd@4.3.0) (2024-02-29)


### Features

* **isOdd:** b params ([d726f93](https://gitlab.com/artyomSultanov/test-lerna/commit/d726f93874a415222c9f1cced6bc6bb8016ff1d3))





# [4.2.0](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@4.0.0...@libs/is-odd@4.2.0) (2024-02-29)


### Features

* **isOdd:** b params ([d726f93](https://gitlab.com/artyomSultanov/test-lerna/commit/d726f93874a415222c9f1cced6bc6bb8016ff1d3))





# [4.1.0](https://gitlab.com/artyomSultanov/test-lerna/compare/@libs/is-odd@4.0.0...@libs/is-odd@4.1.0) (2024-02-29)


### Features

* **isOdd:** b params ([d726f93](https://gitlab.com/artyomSultanov/test-lerna/commit/d726f93874a415222c9f1cced6bc6bb8016ff1d3))
