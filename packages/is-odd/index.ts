import { isEven } from "@libs/is-even"

export const isOdd = (n: number, foo?: string) => !isEven(n) && !foo